/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini D. Simelevicius                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include "examples/topics/multipart/version.h"
#include "config/version.h"
#include "xcept/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"
#include "toolbox/version.h"
#include "api/topics/version.h"

GETPACKAGEINFO(examplestopicsmultipart)

void examplestopicsmultipart::checkPackageDependencies()
{
	CHECKDEPENDENCY(config)
	CHECKDEPENDENCY(xcept)
	CHECKDEPENDENCY(xdata)
	CHECKDEPENDENCY(xdaq)
	CHECKDEPENDENCY(toolbox)
	CHECKDEPENDENCY(apitopics)
}

std::set<std::string, std::less<std::string> > examplestopicsmultipart::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;
	ADDDEPENDENCY(dependencies,config);
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,xdata);
	ADDDEPENDENCY(dependencies,xdaq);
	ADDDEPENDENCY(dependencies,toolbox);
	ADDDEPENDENCY(dependencies,apitopics);
	return dependencies;
}
