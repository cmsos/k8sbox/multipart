/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini D. Simelevicius                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include <chrono>
#include <iterator>

#include "nlohmann/json.hpp"

#include "xcept/tools.h"

#include "examples/topics/multipart/Application.h"

#include "api/topics/Interface.h"
#include "api/topics/exception/Exception.h"


#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/net/URN.h"
#include "toolbox/hexdump.h"

#include "xdata/Properties.h"
#include "xdata/Integer.h"
#include "xdata/exdr/Serializer.h"
#include "xdata/json/Serializer.h"
#include "xdata/exdr/FixedSizeInputStreamBuffer.h"
#include "xdata/exdr/FixedSizeOutputStreamBuffer.h"

XDAQ_INSTANTIATOR_IMPL(examples::topics::multipart::Publisher);
XDAQ_INSTANTIATOR_IMPL(examples::topics::multipart::Subscriber);

using namespace std::placeholders;

examples::topics::multipart::Publisher::Publisher(xdaq::ApplicationStub * s): xdaq::Application(s),connected_(false)
{
	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}

void examples::topics::multipart::Publisher::actionPerformed(xdata::Event& event)
{
	try
	{
		member_ = api::topics::getInterface(this->getApplicationContext())->join(this, this, nullptr); // pool no needed for multipart
	}
	catch (api::topics::exception::Exception & e )
	{
		LOG4CPLUS_INFO(this->getApplicationLogger(), e.what());
		return;
	}
	
	std::thread source_thread(&examples::topics::multipart::Publisher::run, this);
	source_thread.detach();
}

void examples::topics::multipart::Publisher::onConnect(std::shared_ptr<api::topics::Bus> bus)
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "bus " << bus->name() << " is connected");
	connected_ = true;
}

void examples::topics::multipart::Publisher::onDisconnect(std::shared_ptr<api::topics::Bus> bus)
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "bus " << bus->name() << " has diconnected");
	connected_ = false;
}

void examples::topics::multipart::Publisher::run()
{
	std::string originator = this->getApplicationContext()->getContextDescriptor()->getURL();

	toolbox::net::URN urn("topics", "publisher");
	auto pool = toolbox::mem::getMemoryPoolFactory()->createPool(urn, new toolbox::mem::CommittedHeapAllocator(0x100000));

	try
	{
		std::shared_ptr<api::topics::Bus>  bus = member_->busAttach("mosquitto");
		xdata::exdr::Serializer bserializer;

		while(1)
		{
			if (connected_) // to prevent direct error
			{
				try
				{
				toolbox::mem::Reference * ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool,4096);
				auto addr = static_cast<char*>(ref->getDataLocation());

				xdata::exdr::FixedSizeOutputStreamBuffer os(addr, 4096);

				//------- Adding a plist data structure -------------
				xdata::Properties plist;
				plist.setProperty("name", "bril");
				plist.setProperty("system", "lumi");
				plist.setProperty("device_no", "0");
				//Writing into the memory block throught output stream
				bserializer.exportAll(&plist, &os);

				//-------- Adding an integer variable --------------
				xdata::Integer integer;
				integer = 5;
				//Writing into the memory block throught output stream
				bserializer.exportAll(&integer, &os);

				//------- Adding custom binary data ----------------
				double userdata[5] = {1.14159, 2.14159, 3.14159, 4.14159, 5.14159};
				std::memcpy(((char *) ref->getDataLocation()) + os.tellp(), &userdata, sizeof(userdata));

				//Setting the total block size
				ref->setDataSize(os.tellp() + sizeof(userdata));

				// Printing the whole memory block as hexdump
				// std::cout << "data hexdump :" << std::endl;
				// toolbox::hexdump (ref->getDataLocation(), ref->getDataSize());

				bus->publish("examples/topics/multipart/data", ref);

				}
				catch(toolbox::mem::exception::Exception & e)
				{
				    LOG4CPLUS_WARN(this->getApplicationLogger(), "pool is empty" << xcept::stdformat_exception_history(e));
				}
			}
			else
			{
				LOG4CPLUS_WARN(this->getApplicationLogger(), "bus " << bus->name() << " is not connected");
			}
			//std::chrono::milliseconds timespan(2000);
			//std::this_thread::sleep_for(timespan);
		}
	}
	catch(api::topics::exception::Exception & e)
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
	}
}

examples::topics::multipart::Subscriber::Subscriber(xdaq::ApplicationStub * s): xdaq::Application(s)
{
	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	
}
void examples::topics::multipart::Subscriber::onConnect(std::shared_ptr<api::topics::Bus> bus)
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "bus " << bus->name() << " is connected, subscribe to topic ");
	bus->subscribe("examples/topics/multipart/+");
}

void examples::topics::multipart::Subscriber::onDisconnect(std::shared_ptr<api::topics::Bus> bus)
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "bus " << bus->name() << " has diconnected");
}

void examples::topics::multipart::Subscriber::actionPerformed(xdata::Event& event)
{
	try
	{
		toolbox::net::URN urn("topics", "subscriber");
		auto pool = toolbox::mem::getMemoryPoolFactory()->createPool(urn, new toolbox::mem::CommittedHeapAllocator(0x100000));
		
		member_ = api::topics::getInterface(this->getApplicationContext())->join(this, this, pool);
		
		auto cb = std::bind(&examples::topics::multipart::Subscriber::onMessage, this, _1, _2, _3);
		
		std::shared_ptr<api::topics::Bus> bus = member_->busAttach("mosquitto", cb);
		
		//std::chrono::milliseconds timespan(10000);
		//std::this_thread::sleep_for(timespan);
	}
	catch (api::topics::exception::Exception & e )
	{
		LOG4CPLUS_ERROR(this->getApplicationLogger(), xcept::stdformat_exception_history(e));
		return;
	}
	
	
}

void examples::topics::multipart::Subscriber::onMessage(const std::string & topic, std::shared_ptr<api::topics::Bus> bus, toolbox::mem::Reference * ref)
{
	LOG4CPLUS_INFO(this->getApplicationLogger(), "received message for topic '" << topic << "' from bus '" << bus->name() << "' of len " << ref->getDataSize());

	xdata::exdr::FixedSizeInputStreamBuffer is((char*)ref->getDataLocation(), ref->getDataSize());
	
	xdata::Properties plist;
	xdata::Integer integer;
	try
	{
		xdata::exdr::Serializer bserializer;
		bserializer.import(&plist, &is);
		bserializer.import(&integer, &is);
	}
	catch(xdata::exception::Exception & e)
	{
		std::cerr << xcept::stdformat_exception_history(e) << std::endl;
	}

	double userdata[5];
	memcpy(&userdata, ((char *) ref->getDataLocation()) + is.tellp(), sizeof(userdata));

	std::cout << "Received:" << std::endl;
	for (int i = 0; i < 5; i++)
	{
		std::cout << "userdata[" << i << "] = " << userdata[i] << std::endl;
	}

	toolbox::hexdump(ref->getDataLocation(), ref->getDataSize());

	ref->release();
}
