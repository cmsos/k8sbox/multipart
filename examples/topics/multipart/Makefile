#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2023, CERN.                                        #
# All rights reserved.                                                  #
# Authors: L. Orsini, D. Simelevicius                                   #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################


BUILD_HOME:=$(shell pwd)/../../..

BUILD_SUPPORT=build

PROJECT_NAME=core

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

include $(BUILD_HOME)/mfDefs.$(PROJECT_NAME)

#
# Package to be built
#
Project=$(PROJECT_NAME)
Package=examples/topics/multipart

#
# Source files
#
Sources= \
	Application.cc \
	version.cc

#
# Include directories
#
IncludeDirs = \
	$(LOG4CPLUS_INCLUDE_PREFIX) \
	$(CGICC_INCLUDE_PREFIX) \
	$(XCEPT_INCLUDE_PREFIX) \
	$(CONFIG_INCLUDE_PREFIX) \
	$(TOOLBOX_INCLUDE_PREFIX) \
	$(XDAQ_INCLUDE_PREFIX) \
	$(XDATA_INCLUDE_PREFIX) \
	$(XGI_INCLUDE_PREFIX) \
	$(PT_INCLUDE_PREFIX) \
	$(TCPLA_INCLUDE_PREFIX) \
	$(HYPERDAQ_INCLUDE_PREFIX) \
	$(API_TOPICS_INCLUDE_PREFIX) \
	/usr/include/tirpc

LibraryDirs = 

UserSourcePath =

UserCFlags =
UserCCFlags =
UserDynamicLinkFlags =
UserStaticLinkFlags =
UserExecutableLinkFlags =

# These libraries can be platform specific and
# potentially need conditional processing
#
Libraries =
ExternalObjects = 

#
# Compile the source files and create a shared library
#
DynamicLibrary=exampletopicsmultipart
StaticLibrary=

TestLibraries=
TestExecutables=
DependentLibraries=

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.rules
