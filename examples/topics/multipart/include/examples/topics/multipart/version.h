/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini D. Simelevicius                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _examples_topics_multipart_version_h_
#define _examples_topics_multipart_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_EXAMPLESTOPICSMULTIPART_VERSION_MAJOR 1
#define CORE_EXAMPLESTOPICSMULTIPART_VERSION_MINOR 0
#define CORE_EXAMPLESTOPICSMULTIPART_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_EXAMPLESTOPICSMULTIPART_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_EXAMPLESTOPICSMULTIPART_PREVIOUS_VERSIONS


//
// Template macros
//
#define CORE_EXAMPLESTOPICSMULTIPART_VERSION_CODE PACKAGE_VERSION_CODE(CORE_EXAMPLESTOPICSMULTIPART_VERSION_MAJOR,CORE_EXAMPLESTOPICSMULTIPART_VERSION_MINOR,CORE_EXAMPLESTOPICSMULTIPART_VERSION_PATCH)
#ifndef CORE_EXAMPLESTOPICSMULTIPART_PREVIOUS_VERSIONS
#define CORE_EXAMPLESTOPICSMULTIPART_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_EXAMPLESTOPICSMULTIPART_VERSION_MAJOR,CORE_EXAMPLESTOPICSMULTIPART_VERSION_MINOR,CORE_EXAMPLESTOPICSMULTIPART_VERSION_PATCH)
#else 
#define CORE_EXAMPLESTOPICSMULTIPART_FULL_VERSION_LIST  CORE_EXAMPLESTOPICSMULTIPART_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_EXAMPLESTOPICSMULTIPART_VERSION_MAJOR,CORE_EXAMPLESTOPICSMULTIPART_VERSION_MINOR,CORE_EXAMPLESTOPICSMULTIPART_VERSION_PATCH)
#endif 

namespace examplestopicsmultipart
{
	const std::string project = "core";
	const std::string package  =  "examplestopicsmultipart";
	const std::string versions = CORE_EXAMPLESTOPICSMULTIPART_FULL_VERSION_LIST;
	const std::string summary = "Example of application based on topics pakage";
	const std::string description = "The application encapsulates header + data in EXDR format";
	const std::string authors = "Luciano Orsini, Dainius Simelevicius";
	const std::string link = "https://gitlab.cern.ch/cmsos/core/-/wikis/";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
