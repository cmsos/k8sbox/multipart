/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _examples_topics_multipart_Application_h_
#define _examples_topics_multipart_Application_h_

#include "toolbox/mem/Pool.h"
#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "api/topics/Member.h"
#include "api/topics/MemberListener.h"
#include "api/topics/Bus.h"


namespace examples {

namespace topics {

namespace multipart {

class Publisher: public xdaq::Application, public xdata::ActionListener, public api::topics::MemberListener
{
	
	public:

	XDAQ_INSTANTIATOR();
		
	Publisher(xdaq::ApplicationStub * s);
	
	void actionPerformed(xdata::Event& event);
	void onConnect(std::shared_ptr<api::topics::Bus> bus);
	void onDisconnect(std::shared_ptr<api::topics::Bus> bus);

	void run();

	protected:
	
	std::shared_ptr<api::topics::Member> member_;
	std::atomic<bool> connected_;
};

class Subscriber: public xdaq::Application, public xdata::ActionListener, public api::topics::MemberListener
{
	
	public:

	XDAQ_INSTANTIATOR();

	Subscriber(xdaq::ApplicationStub * s);

	void actionPerformed(xdata::Event& event);
	void run();

	// notification of messages upon subscription to a topic
	void onMessage(const std::string & topic, std::shared_ptr<api::topics::Bus> bus, toolbox::mem::Reference * ref);
	void onConnect(std::shared_ptr<api::topics::Bus> bus);
	void onDisconnect(std::shared_ptr<api::topics::Bus> bus);

	protected:

	std::shared_ptr<api::topics::Member> member_;
	toolbox::mem::Pool * pool_;

};
}}}

#endif
