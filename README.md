
# Preparation

on lxplus8.cern.ch

set CLUSTER_NAME="k8sbox"

setenv KUBECONFIG $HOME/cern/openstack/k8sbox/config

kubectl get nodes

kubectl label nodes <node>-# multipart.node.role=true

kubectl label nodes <node>-# broker.node.role=true


# Helm commands

helm repo add multipart-devel https://gitlab.cern.ch/api/v4/projects/167143/packages/helm/devel

helm repo update

helm search repo --devel

helm uninstall multipart

helm install multipart multipart-devel/cmsos-multipart-helm --devel

# kubectl commands

kubectl get pods -n multipart

kubectl get services -n multipart -o wide

kubectl exec -ti <podid> -n multipart -- bash

# browse with manual proxy at localhost 1082

ssh -ND 1082 lxplus.cern.ch

http://k8sbox-bnx7wmqhhrjl-node-2.cern.ch:32000/urn:xdaq-application:lid=50

# Accessing EMQ X Dashboard

Check which nodeport is used by emqx service:

```
$ kubectl describe svc emqx -n multipart | grep NodePort
Type:                     NodePort
NodePort:                 dashboard  31268/TCP
```

Choose one of the nodes of Kubernetes cluster:

```
$ kubectl get nodes
NAME                          STATUS   ROLES    AGE     VERSION
devel-zptgn6ang2vf-master-0   Ready    master   2d19h   v1.25.3
devel-zptgn6ang2vf-node-0     Ready    <none>   2d19h   v1.25.3
devel-zptgn6ang2vf-node-1     Ready    <none>   2d19h   v1.25.3
devel-zptgn6ang2vf-node-2     Ready    <none>   2d19h   v1.25.3
```

Use this URL to open EMQ X Dashboard:

`http://devel-zptgn6ang2vf-node-0:31268`

First time use username: "admin" and password: "public"

# Helm chart instalation in .cms network

On lxplus8 machine:

helm repo add jetstack https://charts.jetstack.io

helm repo update

helm pull jetstack/cert-manager

helm plugin install https://github.com/chartmuseum/helm-push

(using access token called multipart to authenticate, the commands prompts for the token)

helm repo add --username multipart multipart "https://gitlab.cern.ch/api/v4/projects/167143/packages/helm/devel"

helm cm-push cert-manager-v1.12.3.tgz multipart

On machine inside .cms network:

helm repo add multipart-devel https://gitlab.cern.ch/api/v4/projects/167143/packages/helm/devel

helm repo update

helm upgrade --install cert-manager multipart-devel/cert-manager --namespace cert-manager --create-namespace --set installCRDs=true
