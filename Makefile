#########################################################################
# XDAQ Components for Distributed Data Acquisition                      #
# Copyright (C) 2000-2023, CERN.                                        #
# All rights reserved.                                                  #
# Authors: L. Orsini and D. Simelevicius                                #
#                                                                       #
# For the licensing terms see LICENSE.                                  #
# For the list of contributors see CREDITS.                             #
#########################################################################

BUILD_HOME:=$(shell pwd)

ifndef PACKAGES
PACKAGES=examples/topics/multipart
endif
export PACKAGES

ifndef BUILD_SUPPORT
BUILD_SUPPORT=build
endif
export BUILD_SUPPORT

ifndef MFDEFS_SUPPORT
MFDEFS_SUPPORT=1
endif
export MFDEFS_SUPPORT

ifndef PROJECT_NAME
PROJECT_NAME=$(shell pwd | awk -F"/" '{split($$0,a,"/"); print a[NF]}')
endif
export PROJECT_NAME

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules

Project=$(PROJECT_NAME)

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)

Packages=$(PACKAGES)

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.rules

